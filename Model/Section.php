<?php

namespace Cf\Monitor\Model;


use Cf\Monitor\Api\SectionInterface;


class Section implements SectionInterface
{

    /** @var string */
    protected $caption = "Object Data";

    /** @var mixed */
    protected $subject = null;

    /** @var string */
    protected $group = "default";


    /**
     * returns the currently assigned caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * assignes the caption of this section
     *
     * @param string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * returns the currently assigned
     * subject
     *
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * assigns the subject model
     *
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }


    /**
     * returns the group name
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * assigns the group name
     *
     * @param string $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * returns the subject type
     * as string
     *
     * @return string
     */
    public function getType()
    {
        if (!$this->subject) {
            return "null";
        }
        $result = gettype($this->subject);
        if ($result == 'object') {
            $result = get_class($this->subject);
        }
        return $result;
    }


}
