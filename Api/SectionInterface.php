<?php


namespace Cf\Monitor\Api;


interface SectionInterface
{
    /** @return string */
    public function getCaption();

    /** @return string */
    public function getGroup();

    /** @return string */
    public function getType();

    /** @return null | array | stdObject | \Magento\Framework\DataObject */
    public function getSubject();

}


