<?php

namespace Cf\Monitor\Block\Detail;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Api\SortOrder;
use Cf\Monitor\Model\Section;
use Cf\Monitor\Model\SectionFactory;
use Cf\Monitor\Model\Sections;
use Cf\Monitor\Model\SectionsFactory;
use Magento\Framework\View\Element\BlockFactory;
use \Magento\Sales\Model\OrderRepository;

/**
 *
 *
 */
class Order extends \Cf\Monitor\Block\Sections
{
    /** @var \Magento\Sales\Model\OrderRepository */
    protected $orderRepository;

    /** @var SectionFactory */
    protected $sectionFactory;

    /** @var array */
    protected $orders = null;

    /** @var ObjectManagerInterface */
    protected $om;

    /**
     * Cart constructor.
     * @param Context $context
     * @param BlockFactory $blockFactory
     * @param SectionsFactory $sectionsFactory
     * @param SectionFactory $sectionFactory
     * @param CheckoutSession $checkoutSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $om,
        BlockFactory $blockFactory,
        SectionsFactory $sectionsFactory,
        SectionFactory $sectionFactory,
        OrderRepository $orderRepository,
        array $data = [])
    {
        parent::__construct($context, $blockFactory, $sectionsFactory, $data);
        $this->om = $om;
        $this->sectionFactory = $sectionFactory;
        $this->orderRepository = $orderRepository;
    }


    /**
     * fills and prepares the section collection
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addSections(Sections $sections)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $order = $this->getOrder();
        if (!$order) {
            return;
        }

        /** @var Section $section */
        $section = $this->sectionFactory->create($order, __('Order'), 'main');
        $sections->add($section);

        $billing = $order->getBillingAddress();
        if ($billing) {
            $section = $this->sectionFactory->create($billing, __('Billing'), 'main');
            $sections->add($section);
        }

        $shipping = $order->getShippingAddress();
        if ($shipping) {
            $section = $this->sectionFactory->create($shipping, __('Shipping'), 'main');
            $sections->add($section);
        }

        $payment = $order->getPayment();
        if ($payment) {
            $section = $this->sectionFactory->create($payment, __('Payment'), 'main');
            $sections->add($section);
        }

        $items = $order->getItems();
        if ($items && !empty($items)) {
            foreach ($items as $item) {
                $section = $this->sectionFactory->create($item, __('Item'), 'items');
                $sections->add($section);
            }
        }

    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOrder()
    {
        foreach ($this->getOrders() as $order) {
            if ($order->getIsCurrent()) {
                return $order;
            }
        }
        return null;
    }

    /**
     * @return \Magento\Customer\Model\Customer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRequestedOrder()
    {
        if (!isset($this->requestedOrder)) {
            $this->requestedOrder = false;
            $id = (int)$this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $this->requestedOrder = $this->orderRepository->get($id);
                } catch (\Exception $e) {
                    $this->requestedOrder = false;
                }
            }
        }
        return $this->requestedOrder ? $this->requestedOrder : null;
    }


    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOrders()
    {
        if ($this->orders) {
            return $this->orders;
        }

        $this->orders = array();

        /** @var \Magento\Framework\Api\SearchCriteriaBuilder $builder */
        $builder = $this->om->create('Magento\Framework\Api\SearchCriteriaBuilder');
        $builder->setCurrentPage(0);
        $builder->setPageSize(30);

        $sortOrder = $this->om->create('Magento\Framework\Api\SortOrder');
        $sortOrder->setField('entity_id');
        $sortOrder->setDirection(SortOrder::SORT_DESC);
        $builder->setSortOrders([$sortOrder]);
        $critera = $builder->create();

        $requestedOrder = $this->getRequestedOrder();
        $this->orders = $this->orderRepository->getList($critera)->getItems();
        $found = false;
        if ($requestedOrder) {
            foreach ($this->orders as $order) {
                if ($requestedOrder->getId() == $order->getId()) {
                    $found = true;
                    $order->setIsCurrent(true);
                }
            }
            if (!$found) {
                $requestedOrder->setIsCurrent(true);
                array_unshift($this->orders, $requestedOrder);
            }
        } elseif (count($this->orders)) {
            /* get the first array item (associative array) */
            $item = reset($this->orders);
            $item->setIsCurrent(true);
        }
        return $this->orders;
    }


}
