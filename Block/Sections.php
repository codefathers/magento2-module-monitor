<?php

namespace Cf\Monitor\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\BlockFactory;
use Cf\Monitor\Model\SectionsFactory;
use Cf\Monitor\Model\Section;


/**
 *
 *
 */
abstract class Sections extends \Magento\Framework\View\Element\Template
{


    /** @var BlockFactory */
    protected $blockFactory;

    /** @var SectionsFactory */
    protected $sectionsFactory;

    /** @var \Cf\Monitor\Model\Sections */
    protected $sections;


    public function __construct(
        Context $context,
        BlockFactory $blockFactory,
        SectionsFactory $sectionsFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->blockFactory = $blockFactory;
        $this->sectionsFactory = $sectionsFactory;
    }



    /**
     * @return \Cf\Monitor\Model\Sections
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSections()
    {
        if (!$this->sections) {
            $this->sections = $this->sectionsFactory->create();
            $this->addSections($this->sections);
        }
        return $this->sections;
    }


    /**
     * @param \Cf\Monitor\Model\Sections $sections
     * @return mixed
     */
    abstract protected function addSections (\Cf\Monitor\Model\Sections $sections);


    /**
     * @param array $data
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function renderSection(Section $section)
    {
        $block = $this->blockFactory->createBlock('Cf\Monitor\Block\Section');
        $block->setArea('frontend');
        $block->setTemplate('Cf_Monitor::section.phtml');
        $block->setSection($section);
        return $block->toHtml();
    }


}
