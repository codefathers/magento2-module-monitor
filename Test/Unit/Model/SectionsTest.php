<?php


// @codingStandardsIgnoreFile

namespace Cf\Monitor\Test\Unit\Model;

use \Magento\TestFramework\Helper\Bootstrap;


/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @group Cf_Monitor
 *
 */
class SectionsTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $om;

    protected function setUp()
    {
        $this->om = Bootstrap::getObjectManager();
    }

    /**
     *
     * @test
     *
     */
    public function collectionMustHandleSectionModels()
    {
        $sections = $this->om->create('Cf\Monitor\Model\Sections');
        $this->assertInstanceOf('\Cf\Monitor\Model\Sections', $sections);


        $this->assertCount(0, $sections);
        $this->assertEquals(0, $sections->count());

        $section = $this->om->create('Cf\Monitor\Model\Section');
        $section->setCaption('Section 1');
        $section->setGroup('main');
        $sections->add($section);

        $this->assertCount(1, $sections);

        $section = $this->om->create('Cf\Monitor\Model\Section');
        $section->setCaption('Section 2');
        $section->setGroup('default');
        $sections->add($section);

        $this->assertCount(2, $sections);

        $items = $sections->getGroupItems('main');
        $this->assertInstanceOf('\Cf\Monitor\Model\Sections', $items);
        $this->assertCount(1, $items);
        $this->assertContains('main', $items[0]->getGroup());

        $items = $sections->getGroupItems('default');
        $this->assertInstanceOf('\Cf\Monitor\Model\Sections', $items);
        $this->assertCount(1, $items);
        $this->assertContains('default', $items[0]->getGroup());

        $items = $sections->getGroupItems('noname');
        $this->assertInstanceOf('\Cf\Monitor\Model\Sections', $items);
        $this->assertCount(0, $items);
    }



}
