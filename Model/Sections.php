<?php

namespace Cf\Monitor\Model;


use Cf\Monitor\Model\Section;
use Magento\Framework\Data\CollectionDataSourceInterface;


class Sections implements \IteratorAggregate, \Countable, \ArrayAccess, CollectionDataSourceInterface
{


    /** @var array */
    protected $items;

    /**
     * Sections constructor.
     */
    public function __construct()
    {
        $this->clear();
    }

    /**
     * clears all items
     */
    public function clear()
    {
        $this->items = array();
    }


    /**
     * @return \Traversable|void
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * @return int|void
     */
    public function count()
    {
        return count($this->items);
    }


    /**
     * @param \Cf\Monitor\Model\Section $item
     */
    public function add(Section $item)
    {
        $this->items[] = $item;
    }


    /**
     * @return array
     */
    public function getGroups()
    {
        $groups = array();
        foreach ($this as $item) {
            $groups[$item->getGroup()] = 1;
        }
        return array_keys($groups);
    }

    /**
     * @return Sections
     */
    public function getGroupItems($group)
    {
        $group = strtolower($group);
        $result = new self();
        foreach ($this as $item) {
            if ($group == strtolower($item->getGroup())) {
                $result->add($item);
            }
        }
        return $result;
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return ($offset >= 0 && $offset < $this->count());
    }


    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return ($this->offsetExists($offset)) ? $this->items[$offset] : null;
    }


    /**
     * @param mixed $offset
     * @param mixed $value
     * @throws \Exception
     */
    public function offsetSet($offset, $value)
    {
        throw new \Exception("not yet implemented");
    }

    /**
     * @param mixed $offset
     * @throws \Exception
     */
    public function offsetUnset($offset)
    {
        throw new \Exception("not yet implemented");
    }

}
