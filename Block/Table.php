<?php

namespace Cf\Monitor\Block;


use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\BlockFactory;

/**
 *
 *
 */
class Table extends \Magento\Framework\View\Element\Template
{

    /** @var BlockFactory */
    protected $blockFactory;


    /**
     * @var int
     */
    protected $rowIdx = 0;


    /**
     * Constructor
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        BlockFactory $blockFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->blockFactory = $blockFactory;
    }


    /**
     *
     * @throws \Exception
     */
    public function renderSubject($subject)
    {
        if (is_array($subject)) {
            return $this->renderArrayTable($subject);
        } elseif ($subject instanceof \Magento\Framework\DataObject) {
            return $this->renderModelTable($subject);
        } elseif ($subject instanceof \stdClass) {
            return $this->renderObjectTable($subject);
        } elseif (is_array($subject)) {
            return $this->renderArrayTable();
        }
        throw new \Exception(sprintf('Invalid model %s', get_class($subject)));
    }



    /**
     * @param $value
     * @return string
     */
    public function literalToString($value)
    {
        $result = $value;
        if (is_object($value)) {
            $result = get_class($value);
        } elseif (is_array($value)) {
            $cnt = count($value);
            $type = $this->decorateLiteral("array ($cnt)");
            $result = "$type<br />" . $this->renderSubject($value);
        } elseif (is_string($value)) {
            $obj = @unserialize($value);
            if ($obj!==false) {
                $result = $this->literalToString($obj);
            } else {
                $str = $this->escapeHtml((string)$value);
                $result = (strlen($value))?wordwrap($str,50,'<i style="color:gray;">&#8629;</i><br />',true):"&nbsp;";
            }
        } elseif (is_bool($value)) {
            $result = (string)$this->boolToString($value);
        } elseif (is_int($value)) {
            $result = (string)$value;
        } elseif (is_numeric($value)) {
            $result = (string)$value;
        } elseif (is_null($value)) {
            $result = $this->decorateLiteral('null');
        }

        return $result;
    }


    protected function decorateLiteral($value)
    {
        return '<i style="color:gray;">'.$this->escapeHtml($value).'</i>';
    }

    /**
     * @param string $val
     * @return bool
     */
    protected function stringToBool($val)
    {
        $val = strtolower((string)$val);
        if ($val == '1' || $val == 'true' || $val == 'yes' || $val == 'enabled') {
            return true;
        }
        if ($val == '0' || $val == 'false' || $val == 'no' || $val == 'disabled') {
            return false;
        }
        return false;
    }


    /**
     * @param $val
     * @return string
     */
    protected function boolToString($val)
    {
        $val = (bool)$val;
        if ($val === true) {
            return 'true';
        }
        return 'false';
    }


    /**
     *
     */
    public function getRowClass()
    {
        return ($this->rowIdx++ % 2) ? "odd" : "even";
    }



    /**
     *
     */
    protected function renderObjectTable($model)
    {
        $meta = array(
            'CLASS' => get_class($model)
        );
        $arr = $meta + (array)$model;
        return $this->renderArrayTable($arr);
    }

    /**
     *
     */
    protected function renderModelTable(\Magento\Framework\DataObject $model)
    {
        $meta = array(
            'CLASS' => get_class($model),
            'ID' => $model->getId()
        );
        $arr = $meta + $model->getData();
        return $this->renderArrayTable($arr);
    }



    /**
     *
     */
    protected function renderArrayTable($arr)
    {
        $block = $this->blockFactory->createBlock('Cf\Monitor\Block\Table');
        $block->setArea('frontend');
        $block->setTemplate('Cf_Monitor::table.phtml');
        $block->setSubject($arr);
        return $block->toHtml();
    }


}
