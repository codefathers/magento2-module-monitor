# Cf-Monitor - Magento 2 Module

Shows attribute values of common models, like cart, the current customer or particular orders. 
 
The module provides controller endpoints to visualize the state of internal models as simple key/value tables.


![Screenshot](Doc/preview.png)


Just call..

~~~
=> http[s]://[SHOP_DOMAIN]/monitor
~~~

.. and browse.  
 
 
 **NOTICE**: this module is meant as a developer tool just to help getting insights into runtime models and should never been installed on PRODUCTION environments. So: only link this module in the 'require-dev' section of your composer file.
 
 