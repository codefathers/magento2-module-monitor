<?php

namespace Cf\Monitor\Block\Detail;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\BlockFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Api\SortOrder;
use Cf\Monitor\Model\Section;
use Cf\Monitor\Model\SectionFactory;
use Cf\Monitor\Model\Sections;
use Cf\Monitor\Model\SectionsFactory;


/**
 *
 *
 */
class Customer extends \Cf\Monitor\Block\Sections
{

    /** @var CustomerSession */
    protected $customerSession;

    /** @var SectionFactory */
    protected $sectionFactory;

    /** @var CustomerRepository */
    protected $customerRepository;

    /** @var \Magento\Customer\Model\CustomerRegistry */
    protected $customerRegistry;

    /** @var \Magento\Customer\Model\Customer */
    protected $requestedCustomer;

    /** @var ObjectManagerInterface */
    protected $om;

    /** @var array */
    protected $customers = null;

    /**
     * Customer constructor.
     * @param Context $context
     * @param BlockFactory $blockFactory
     * @param SectionsFactory $sectionsFactory
     * @param SectionFactory $sectionFactory
     * @param CustomerSession $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $om,
        BlockFactory $blockFactory,
        SectionsFactory $sectionsFactory,
        SectionFactory $sectionFactory,
        CustomerSession $customerSession,
        CustomerRepository $customerRepository,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        array $data = [])
    {
        parent::__construct($context, $blockFactory, $sectionsFactory, $data);
        $this->om = $om;
        $this->sectionFactory = $sectionFactory;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->customerRegistry = $customerRegistry;
    }


    /**
     * fills and prepares the section collection
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addSections(Sections $sections)
    {
        /** @var \Magento\Customer\Model\Customer $customer */
        $customer = $this->getCustomer();
        if (!$customer) {
            return;
        }

        /** @var Section $section */
        $section = $this->sectionFactory->create($customer, __('Customer'), 'main');
        $sections->add($section);

        $addresses = $customer->getAddresses();
        foreach ($addresses as $address) {
            $section = $this->sectionFactory->create($address, __('Address'), 'addresses');
            $sections->add($section);
        }

    }


    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getSessionCustomer()
    {
        $result = $this->customerSession->getCustomer();
        return $result && $result->getId() ? $result : null;
    }

    /**
     * @return \Magento\Customer\Model\Customer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRequestedCustomer()
    {
        if (!isset($this->requestedCustomer)) {
            $this->requestedCustomer = false;
            $id = (int)$this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $this->requestedCustomer = $this->customerRepository->getById($id);
                } catch (\Exception $e) {
                    $this->requestedCustomer = false;
                }
            }
        }
        return $this->requestedCustomer ? $this->requestedCustomer : null;
    }


    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        /** @var  $customers */
        foreach ($this->getCustomers() as $item) {
            if ($item->getIsCurrent()) {
                return $item->getSubject();
            }
        }
        return null;
    }


    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomers()
    {
        if ($this->customers) {
            return $this->customers;
        }

        $this->customers = array();

        $createItem = function ($customer, $isCurrent = false, $isSession = false) {
            if (!$customer instanceof \Magento\Framework\DataObject) {
                $customer = $this->customerRegistry->retrieve($customer->getId());
            }
            $data = array(
                'id' => $customer->getId(),
                'subject' => $customer,
                'name' => sprintf("%s %s", $customer->getFirstname(), $customer->getLastname()),
                'label' => sprintf("%s - %s %s", $customer->getId(), $customer->getFirstname(), $customer->getLastname()),
                'is_current' => $isCurrent,
                'is_session' => $isSession
            );
            $factory = $this->om->get(\Magento\Framework\DataObjectFactory::class);
            return $factory->create(array('data' => $data));
        };

        /** @var \Magento\Framework\Api\SearchCriteriaBuilder $builder */
        $builder = $this->om->create('Magento\Framework\Api\SearchCriteriaBuilder');
        $builder->setCurrentPage(0);
        $builder->setPageSize(30);

        $sortOrder = $this->om->create('Magento\Framework\Api\SortOrder');
        $sortOrder->setField('entity_id');
        $sortOrder->setDirection(SortOrder::SORT_DESC);
        $builder->setSortOrders([$sortOrder]);
        $critera = $builder->create();

        $sessionCustomer = $this->getSessionCustomer();
        $requestedCustomer = $this->getRequestedCustomer();
        $currentCustomer = $requestedCustomer ? $requestedCustomer : $sessionCustomer;

        $hasSession = false;
        $hasCurrent = false;
        $items = $this->customerRepository->getList($critera)->getItems();
        foreach ($items as $customer) {
            $hasCurrent = $hasCurrent || ($currentCustomer && ($customer->getId() == $currentCustomer->getId()));
            $hasSession = $hasSession || ($sessionCustomer && ($customer->getId() == $sessionCustomer->getId()));
            $this->customers[] = $createItem($customer, false, false);
        }
        if (!$hasCurrent && $currentCustomer) {
            $customer = $createItem($currentCustomer);
            array_unshift($this->customers, $customer);
        };
        if (!$hasSession && $sessionCustomer && $sessionCustomer !== $currentCustomer) {
            $customer = $createItem($sessionCustomer);
            array_unshift($this->customers, $customer);
        };
        foreach ($this->customers as $item) {
            if ($currentCustomer && $item->getId() == $currentCustomer->getId()) {
                $item->setIsCurrent(true);
            }
            if ($sessionCustomer && $item->getId() == $sessionCustomer->getId()) {
                $item->setIsSession(true);
            }
        }
        return $this->customers;
    }


}
