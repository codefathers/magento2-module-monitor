<?php


// @codingStandardsIgnoreFile

namespace Cf\Monitor\Test\Unit\Block;

use \Magento\TestFramework\Helper\Bootstrap;


/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @group Cf_Monitor
 *
 */
class SectionsTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $om;

    /**
     * @var \Magento\Framework\View\Element\BlockFactory
     */
    private $blockFactory;


    /**
     *
     */
    protected function setUp()
    {
        $this->om = Bootstrap::getObjectManager();
        $this->blockFactory = $this->om->get(\Magento\Framework\View\Element\BlockFactory::class);
    }


    /**
     *
     * @test
     *
     */
    public function renderArraySubjectMustReturnHtmlTable()
    {

        /** @var  \Cf\Monitor\Block\Table $block */
        $block = $this->blockFactory->createBlock('Cf\Monitor\Block\Table');
        $this->assertInstanceOf('\Cf\Monitor\Block\Table', $block);

        $data = array(
            'foo' => 'bar',
            'baz' => 'foo',
        );
        $html = $block->renderSubject($data);
        $this->assertInternalType('string', $html);
        $this->assertNotEmpty($html);
        $this->assertContains('<table', $html);
        $this->assertContains('foo', $html);
        $this->assertContains('bar', $html);
        $this->assertContains('baz', $html);
        $this->assertContains('class="even"', $html);
        $this->assertContains('class="odd"', $html);
    }

    /**
     *
     * @test
     *
     */
    public function renderStdClassSubjectMustReturnHtmlTable()
    {

        /** @var  \Cf\Monitor\Block\Table $block */
        $block = $this->blockFactory->createBlock('Cf\Monitor\Block\Table');
        $this->assertInstanceOf('\Cf\Monitor\Block\Table', $block);

        $obj = new \stdClass();
        $obj->foo = 'bar';
        $obj->baz = 'foo';

        $html = $block->renderSubject($obj);
        $this->assertInternalType('string', $html);
        $this->assertNotEmpty($html);
        $this->assertContains('<table', $html);
        $this->assertContains('foo', $html);
        $this->assertContains('bar', $html);
        $this->assertContains('baz', $html);
        $this->assertContains('class="even"', $html);
        $this->assertContains('class="odd"', $html);
        $this->assertContains('stdClass', $html);
    }

    /**
     *
     * @test
     *
     */
    public function renderModelSubjectMustReturnHtmlTable()
    {

        /** @var  \Cf\Monitor\Block\Table $block */
        $block = $this->blockFactory->createBlock('Cf\Monitor\Block\Table');
        $this->assertInstanceOf('\Cf\Monitor\Block\Table', $block);

        $model = new \Magento\Framework\DataObject();
        $model->setFoo('bar');
        $model->setBaz('baz');
        $model->setId(12345);

        $html = $block->renderSubject($model);
        $this->assertInternalType('string', $html);
        $this->assertNotEmpty($html);
        $this->assertContains('<table', $html);
        $this->assertContains('foo', $html);
        $this->assertContains('bar', $html);
        $this->assertContains('baz', $html);
        $this->assertContains('class="even"', $html);
        $this->assertContains('class="odd"', $html);
        $this->assertContains('12345', $html);
        $this->assertContains('Magento\Framework\DataObject', $html);
        $this->assertContains('CLASS', $html);
        $this->assertContains('ID', $html);
    }


}
