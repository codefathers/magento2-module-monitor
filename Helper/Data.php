<?php


namespace Cf\Monitor\Helper;


/**
 *
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{


    /**
     * estimates the type of a given object and returns it
     * as a string
     *
     * @param mixed $obj
     * @return string
     */
    public function getTypeString($obj)
    {
        $result = gettype($obj);
        if ($result == 'object') {
            $result = "class " . get_class($obj);
        }
        return $result;
    }




}
