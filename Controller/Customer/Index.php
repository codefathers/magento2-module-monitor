<?php


namespace Cf\Monitor\Controller\Customer;

use Magento\Framework\App\Action\Action;


class Index extends Action
{

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->_forward('details');
    }

}
