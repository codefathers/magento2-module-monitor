<?php

namespace Cf\Monitor\Block\Detail;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Api\SortOrder;
use Cf\Monitor\Model\Section;
use Cf\Monitor\Model\SectionFactory;
use Cf\Monitor\Model\Sections;
use Cf\Monitor\Model\SectionsFactory;
use Magento\Framework\View\Element\BlockFactory;
use \Magento\Sales\Model\OrderRepository;

/**
 *
 *
 */
class Session extends \Cf\Monitor\Block\Sections
{
    /** @var SectionFactory */
    protected $sectionFactory;

    /** @var \Magento\Framework\DataObject */
    protected $session = null;

    /** @var ObjectManagerInterface */
    protected $om;

    /**
     * Cart constructor.
     * @param Context $context
     * @param BlockFactory $blockFactory
     * @param SectionsFactory $sectionsFactory
     * @param SectionFactory $sectionFactory
     * @param CheckoutSession $checkoutSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $om,
        BlockFactory $blockFactory,
        SectionsFactory $sectionsFactory,
        SectionFactory $sectionFactory,
        array $data = [])
    {
        parent::__construct($context, $blockFactory, $sectionsFactory, $data);
        $this->om = $om;
        $this->sectionFactory = $sectionFactory;
    }


    /**
     * fills and prepares the section collection
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addSections(Sections $sections)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $session = $this->getSession();
        $baseSection = $this->sectionFactory->create($session, __('Session'), 'main');
        $sections->add($baseSection);

        $baseKeys = array(
            'CLASS',
            'ID'
        );
        foreach ($session->getData() as $key => $value) {
            if (is_array($value) and !in_array($key, $baseKeys) and $key[0] !== '_') {
                $section = $this->sectionFactory->create(array_merge($value), __("Namespace '$key'"), 'subs');
                $sections->add($section);
                $session->unsetData($key);
            }
        }
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSession()
    {
        if (!$this->session) {
            $this->session = new \Magento\Framework\DataObject(array_merge($_SESSION));
        }
        return $this->session;
    }


}
