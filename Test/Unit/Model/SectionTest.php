<?php


// @codingStandardsIgnoreFile

namespace Cf\Monitor\Test\Unit\Model;

use \Magento\TestFramework\Helper\Bootstrap;


/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @group Cf_Monitor
 *
 */
class SectionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $om;

    protected function setUp()
    {
        $this->om = Bootstrap::getObjectManager();
    }

    /**
     *
     * @test
     *
     */
    public function factoryMustCreateSectionModel()
    {
        $section = $this->om->create('Cf\Monitor\Model\Section');
        $this->assertInstanceOf('\Cf\Monitor\Model\Section', $section);

        $factory = $this->om->create('Cf\Monitor\Model\SectionFactory');
        $section = $factory->create(null);
        $this->assertInstanceOf('\Cf\Monitor\Model\Section', $section);

    }



    /**
     *
     * @test
     *
     */
    public function factoryMustCreateSectionFromModel()
    {
        $obj = $this->om->create('Magento\Framework\DataObject');

        $factory = $this->om->create('Cf\Monitor\Model\SectionFactory');
        $section = $factory->create($obj, 'UnitTest 1', 'group-1');
        $this->assertInstanceOf('\Cf\Monitor\Model\Section', $section);
        $this->assertEquals('group-1', $section->getGroup());
        $this->assertEquals('UnitTest 1', $section->getCaption());
        $this->assertEquals('Magento\Framework\DataObject', $section->getType());

        $subject = $section->getSubject();
        $this->assertSame($obj, $subject);
    }

    /**
     *
     * @test
     *
     */
    public function factoryMustCreateSectionFromArray()
    {
        $arr = array(
            'id' => 't-011',
            'name' => 'unit test',
            'foo' => 'bar',
            'baz_zim' => 'don'
        );
        $factory = $this->om->create('Cf\Monitor\Model\SectionFactory');
        $section = $factory->create($arr, 'UnitTest 2', 'group-2');

        $this->assertInstanceOf('\Cf\Monitor\Model\Section', $section);
        $this->assertEquals('group-2', $section->getGroup());
        $this->assertEquals('UnitTest 2', $section->getCaption());
        $this->assertEquals('array', $section->getType());

        $subject = $section->getSubject();
        $this->assertSame($arr, $subject);
    }

    /**
     *
     * @test
     *
     */
    public function factoryMustCreateSectionFromObject()
    {
        $obj = new \stdClass();
        $obj->foo = 'bar';
        $obj->id = 1234567;

        $factory = $this->om->get('Cf\Monitor\Model\SectionFactory');
        $section = $factory->create($obj, 'UnitTest 3', 'group-3');

        $this->assertInstanceOf('\Cf\Monitor\Model\Section', $section);
        $this->assertEquals('group-3', $section->getGroup());
        $this->assertEquals('UnitTest 3', $section->getCaption());
        $this->assertEquals('stdClass', $section->getType());

        $subject = $section->getSubject();
        $this->assertSame($obj, $subject);
    }

}
