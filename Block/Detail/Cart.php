<?php

namespace Cf\Monitor\Block\Detail;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\ObjectManagerInterface;
use Cf\Monitor\Model\Section;
use Cf\Monitor\Model\SectionFactory;
use Cf\Monitor\Model\Sections;
use Cf\Monitor\Model\SectionsFactory;
use Magento\Framework\View\Element\BlockFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Api\SortOrder;

/**
 *
 *
 */
class Cart extends \Cf\Monitor\Block\Sections
{
    /** @var \Magento\Checkout\Model\Session */
    protected $checkoutSession;

    /** @var SectionFactory */
    protected $sectionFactory;

    /** @var SectionFactory */
    protected $quoteRepository;

    /** @var ObjectManagerInterface */
    protected $om;

    /** @var \Magento\Quote\Model\Quote */
    protected $requestedQuote = null;

    /**
     * Cart constructor.
     * @param Context $context
     * @param BlockFactory $blockFactory
     * @param SectionsFactory $sectionsFactory
     * @param SectionFactory $sectionFactory
     * @param CheckoutSession $checkoutSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $om,
        BlockFactory $blockFactory,
        SectionsFactory $sectionsFactory,
        SectionFactory $sectionFactory,
        CheckoutSession $checkoutSession,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        array $data = [])
    {
        parent::__construct($context, $blockFactory, $sectionsFactory, $data);
        $this->om = $om;
        $this->sectionFactory = $sectionFactory;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
    }


    /**
     * fills and prepares the section collection
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addSections(Sections $sections)
    {

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->getQuote();
        if (!$quote) {
            return;
        }

        /** @var Section $section */
        $section = $this->sectionFactory->create($quote, __('Quote'), 'main');
        $sections->add($section);

        $billing = $quote->getBillingAddress();
        $section = $this->sectionFactory->create($billing, __('Billing'), 'main');
        $sections->add($section);

        $shipping = $quote->getShippingAddress();
        $section = $this->sectionFactory->create($shipping, __('Shipping'), 'main');
        $sections->add($section);

        $payment = $quote->getPayment();
        $section = $this->sectionFactory->create($payment, __('Payment'), 'main');
        $sections->add($section);

        $items = $quote->getItems();
        if ($items && !empty($items)) {
            foreach ($quote->getItems() as $item) {
                $section = $this->sectionFactory->create($item, __('Item'), 'items');
                $sections->add($section);
            }
        }

    }


    /**
     * @return \Magento\Quote\Model\Quote
     */
    public function getSessionQuote()
    {
        return $this->checkoutSession->getQuote();
    }

    /**
     * @return \Magento\Quote\Api\Data\CartInterface|\Magento\Quote\Model\Quote
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRequestedQuote()
    {
        if (!isset($this->requestedQuote)) {
            $this->requestedQuote = false;
            $id = $this->getRequestedQuoteId();
            if ($id) {
                try {
                    $this->requestedQuote = $this->quoteRepository->get($id);
                } catch (\Exception $e) {
                    $this->requestedQuote = false;
                }
            }
        }
        return $this->requestedQuote ? $this->requestedQuote : null;
    }


    /**
     * @return int
     */
    public function getRequestedQuoteId()
    {
        return (int) $this->getRequest()->getParam('id');
    }


    /**
     *
     */
    public function isValidRequest()
    {
        if ($this->getRequestedQuoteId() && !$this->getRequestedQuote()) {
            return false;
        }
        return true;
    }


    /**
     * @return \Magento\Quote\Api\Data\CartInterface|\Magento\Quote\Model\Quote
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getQuote()
    {
        $result = $this->getRequestedQuote();
        if (!$result) {
            $result = $this->getSessionQuote();
        }
        return $result;
    }

    /**
     *
     */
    public function getQuotes()
    {
        $sessionQuote = $this->getSessionQuote();
        $quote = $this->getQuote();

        /** @var \Magento\Framework\Api\SearchCriteriaBuilder $builder */
        $builder = $this->om->create('Magento\Framework\Api\SearchCriteriaBuilder');
        $builder->setCurrentPage(0);
        $builder->setPageSize(20);

        $sortOrder = $this->om->create('Magento\Framework\Api\SortOrder');
        $sortOrder->setField('entity_id');
        $sortOrder->setDirection(SortOrder::SORT_DESC);
        $builder->setSortOrders([$sortOrder]);
        $critera = $builder->create();

        $quotes = $this->quoteRepository->getList($critera)->getItems();
        foreach ($quotes as $item) {
            if ($sessionQuote && ($item->getId() == $sessionQuote->getId())) {
                $item->setIsSession(true);
            }
            if ($quote && ($item->getId() == $quote->getId())) {
                $item->setIsCurrent(true);
            }
        }
        return $quotes;
    }


}
