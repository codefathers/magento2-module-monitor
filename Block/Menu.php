<?php

namespace Cf\Monitor\Block;


use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\BlockFactory;

/**
 *
 *
 */
class Menu extends \Magento\Framework\View\Element\Template
{

    /** @var BlockFactory */
    protected $blockFactory;


    /**
     * @var int
     */
    protected $rowIdx = 0;


    /**
     * Constructor
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        BlockFactory $blockFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->blockFactory = $blockFactory;
    }


}
