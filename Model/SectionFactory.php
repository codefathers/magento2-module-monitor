<?php

namespace Cf\Monitor\Model;


use Cf\EnvTool\Exception;
use Cf\Monitor\Model\Section;
use Magento\Framework\DataObject;


class SectionFactory
{

    /** @var string */
    const ITEM_CLASS = Section::class;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;


    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param $subject
     * @param string $caption
     * @param string $group
     * @return mixed
     */
    public function create($subject, $caption = '', $group = 'default')
    {
        $result = $this->objectManager->create(self::ITEM_CLASS);
        $result->setSubject($subject);
        $result->setCaption($caption);
        $result->setGroup($group);
        return $result;
    }


}
